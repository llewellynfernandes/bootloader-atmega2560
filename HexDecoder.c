
#include "HexDecoder.h"


void DecodeHex(char *String , Hex *Var){
	char temp[4] , *ptr;
	uint8_t point = 0;
	if(String[0] == ':'){
		memset(temp , 0 , sizeof(temp));

		for(int i = 0; i<2; i++)
			temp[i] = String[i+1];

		Var->RecordLen = strtol(temp , &ptr , 16);

		memset(temp , 0 , sizeof(temp));

		for(int i = 0; i<4; i++)
			temp[i] = String[i+3];

		Var->Address = strtol(temp , &ptr , 16);

		memset(temp , 0 , sizeof(temp));

		for(int i = 0; i<2; i++)
			temp[i] = String[i+7];

		Var->DataType  = strtol(temp , &ptr , 16);
		point = 9;

		memset(Var->data , 0 , sizeof(Var->data));
		if(Var->RecordLen && !Var->DataType){

			for(int i = 0 ; i < Var->RecordLen; i++){
				for(int j =0 ; j < 2 ; j++){
					temp[j] = String[point++];
				}
				Var->data[i] = strtol(temp , &ptr , 16);
				memset(temp , 0 , sizeof(temp));
			}

		}
		memset(temp , 0 , sizeof(temp));
		temp[0] = String[point];
		temp[1] = String[point + 1];
		Var->Checksum = strtol(temp , &ptr , 16);
	}

	//Call Funciton to Write HEx to memory.
	printf("Testing 123\r\n");

}